<?php
class Pet
{
	public static $breeds = array("", "", "", "", "");
	public static $type = "";
	public static $ages = array("Младше 3-х месяцев", "3-12 месяцев", "Старше 1-го года");
	public static $colors = array("Другой цвет", "Чёрный", "Серый", "Белый", "Рыжий");
}

class Cat extends Pet
{
	public static $type = "Кошка";
	public static $breeds = array("Неизвестный", "Сибирская", "Мейнкун", "Персидская", "Британская");
}

class Dog extends Pet
{
	public static $type = "Собака";
	public static $breeds = array("Неизвестный", "Овчарка", "Такса", "Пудель", "Бульдог");
}

class Rodent extends Pet
{
	public static $type = "Грызун";
	public static $breeds = array("Неизвестный", "Крыса", "Хомяк", "Морская свинка", "Кролик");
}

class Bird extends Pet
{
	public static $type = "Птица";
	public static $breeds = array("Неизвестный", "Голубь", "Воробей", "Попугай", "Ворон");
}
?>