﻿<?php
	include 'conf.php';
	include 'pets.php';

	// Соединение, выбор базы данных
	$dbconn = pg_connect("host=".$host." dbname=".$database." user=".$username." password=".$password)
		or die('Ошибка подключения: ' . pg_last_error());

	// Выполнение SQL запроса
	$query  = 'INSERT INTO "pets" ("id", "type", "coords", "address", "breed", "age", "color", "description", "img") ';
	$query .= 'VALUES (nextval(\'"pets_id_seq"\'::regclass),\''.$_POST["add_pet_type"].'\',\''.$_POST["add_coordinates"].'\',\''.$_POST["add_address"].'\'';
	$query .= ',\''.$_POST["add_breed"].'\',\''.$_POST["add_age"].'\',\''.$_POST["add_color"].'\',\''.$_POST["add_description"].'\',\''.$_POST["add_img_url"].'\')';
	
	$result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());
	
	// Очистка результата
	pg_free_result($result);

	// Закрытие соединения
	pg_close($dbconn);	
?>	