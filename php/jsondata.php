﻿<?php
	include 'conf.php';
	include 'pets.php';

	// Соединение, выбор базы данных
	$dbconn = pg_connect("host=".$host." dbname=".$database." user=".$username." password=".$password)
		or die('Ошибка подключения: ' . pg_last_error());

	// Выполнение формирование SQL запроса и JSON строки с ответом
	$jsonstr = '{"type": "FeatureCollection", "features": [';
	
	$query = 'SELECT * FROM pets';
	$result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());
	
	// Костыльно, но работает
	while ($arr = pg_fetch_array($result, null, PGSQL_ASSOC))
	{
		$arr['coords'] = str_replace("(", "[", $arr['coords']);
		$arr['coords'] = str_replace(")", "]", $arr['coords']);		
		
		// Определяем тип питомца
		$type; $breed; $age; $color; $preset;
		
		if ($arr['type'] == 0) // Кошка
		{
			$breed = Cat::$breeds[$arr['breed']];
			$age   = Cat::$ages[$arr['age']];
			$color = Cat::$colors[$arr['color']];
			$type  = Cat::$type;
			$preset = "islands#greenDotIcon";
		}
		elseif ($arr['type'] == 1)	// Собака
		{
			$breed = Dog::$breeds[$arr['breed']];
			$age   = Dog::$ages[$arr['age']];
			$color = Dog::$colors[$arr['color']];
			$type  = Dog::$type;
			$preset = "islands#blueDotIcon";
		}
		elseif ($arr['type'] == 2)	// Грызун
		{
			$breed = Rodent::$breeds[$arr['breed']];
			$age   = Rodent::$ages[$arr['age']];
			$color = Rodent::$colors[$arr['color']];
			$type  = Rodent::$type;
			$preset = "islands#orangeDotIcon";
		}
		elseif ($arr['type'] == 3)	// Птица
		{
			$breed = Bird::$breeds[$arr['breed']];
			$age   = Bird::$ages[$arr['age']];
			$color = Bird::$colors[$arr['color']];
			$type  = Bird::$type;
			$preset = "islands#violetDotIcon";
		}
					
		$jsonstr .= '{"type": "Feature", "id": '.$arr['id'].', "geometry": {"type": "Point", "coordinates": '.$arr['coords'].'}, "properties":';
		$jsonstr .= '{"balloonContent": "<img src=\''.$arr['img'].'\' class=\'rounded center-block img-fluid\' alt=\'Изображение отсутствует\'>';
		$jsonstr .= '<br><b>Вид: </b>'.$breed.'<br><b>Возраст: </b>'.$age.'<br><b>Цвет: </b>'.$color.'<br><b>Адрес: </b>'.$arr['address'].'<br>';
		$jsonstr .= '<b>Описание: </b>'.$arr['description'].'", "clusterCaption": "'.$type.'", "hintContent": "'.$breed.'", "type": "'.$arr['type'].'", "breed": "'.$arr['breed'].'", "age": "'.$arr['age'].'", ';
		$jsonstr .= '"color": "'.$arr['color'].'", "address": "'.$arr['address'].'"}, "options": {"preset": "'.$preset.'"}},';	
	}

	// Удаляем запятую и закрываем JSON строку
	$jsonstr = substr($jsonstr, 0, -1);
	$jsonstr .= ']}';
	
	// Очистка результата
	pg_free_result($result);

	// Закрытие соединения
	pg_close($dbconn);	
	
	echo $jsonstr;
?>