﻿/** Функция округления по Гауссу
	@param num - число
	@param decimalPlaces - кол-во знаков после запятой
*/
function gaussRound(num, decimalPlaces) 
{
	var d = decimalPlaces || 0,
	m = Math.pow(10, d),
	n = +(d ? num * m : num).toFixed(8),
	i = Math.floor(n), f = n - i,
	e = 1e-8,
	r = (f > 0.5 - e && f < 0.5 + e) ?
		((i % 2 == 0) ? i : i + 1) : Math.round(n);
	return d ? r / m : r;
}        

/** Функция конвертации координат из Географических в Прямоугольные
	@param lat - долгота (в градусах)
	@param lng - широта (в градусах)
	@return [X, Y] - прямоугольные координаты
*/
function convertFromGCtoRC(lat, lng)
{
	var e2=0.00669438,
	a=6378137,
	e21=e2/(1-e2), 
	c=a*Math.sqrt(1+e21),
	B=lat / 57.29577951308232,
	L=lng / 57.29577951308232,
	H=120,	// Ср. высота города Новосибирска в метрах
	V=Math.sqrt(1+e21*Math.pow(Math.cos(B),2)), N=gaussRound(c/V,3);
	var X=gaussRound((N+H)*Math.cos(B)*Math.cos(L),3),
	Y=gaussRound((N+H)*Math.cos(B)*Math.sin(L),3);
	
	return [X, Y];
}

/** Функция получения случайного числа в диапазоне */
function getRandomArbitary(min, max)
{
	return Math.random()*(max-min)+min;
}

/** Кастомный алерт в правом нижнем углу карты
	@param type - тип алерта (см. документацию bootstrap 4)
	@param title - заголовок
	@param msg - сообщение
 */
function showAlert(type, title, msg)
{
	$("#main-panel").after(
		'<div class="alert ' + type + '" role="alert" style="display: none; z-index: 1; position: absolute; bottom: 20px;">' +
			'<strong>'+title+'</strong> ' + msg +
		'</div>'
	);
	
	$(".alert").css('left', ($(window).width()-$(".alert").width()-57)+'px');
	$(".alert").fadeIn("slow");
	
	setTimeout(function()
	{
		$(".alert").fadeOut("slow", function(){$(this).remove()});
	}, 5000);
}