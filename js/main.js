﻿ymaps.ready(init);

var myMap, objManager, myPlacemarks, myCircle, myPolygon, 
	breeds;	// Породы понадобятся не только в init()

/** Инициализирует карту и добавляет нужные обработчики */
function init()	
{
	myMap = new ymaps.Map("map", 
	{
		center: [54.9972, 82.9123],
		zoom: 13,
		controls:  []
	},
	{
		balloonMaxWidth: 400	
	});
	
	// ObjectManager принимает те же опции, что и кластеризатор
	// https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/ObjectManager-docpage/
	objManager = new ymaps.ObjectManager(	
	{
		clusterize: true,
		gridSize: 64
	});
	
	$.ajax(
	{
		url: 'php/jsondata.php'
	}).done(function(data)
	{
		myPlacemarks = data;
		objManager.add(myPlacemarks);
	});
	// Цвет кластеризатора - серый
	objManager.clusters.options.set('preset', 'islands#greyClusterIcons');	
	// Добавляем objManager на карту
	myMap.geoObjects.add(objManager);
	
	// Инициализируем массивы, хранящие атрибуты питомцев
	// За одно заполняем фильтры
	var types, ages, colors;	
	$.post('php/pet-type.php', function(result) 
	{ 
		types = JSON.parse(result);
		
		// Заполняем список типов питомцев
		$.each(types, function(key, value)
		{
			$('#search-type').append($("<option></option>").attr("value", key).attr("selected","selected").text(value)); 
		});
		
		for (var i = 0; i < types.length; ++i)
		{
			$("#form-search-animal").append(
				'<div class="additional-filter-left-column">' +
					'<span>' + types[i] + ':</span>' +
				'</div>');
			$("#form-search-animal").append(
				'<div class="additional-filter-right-column">' +
					'<select class="search-breed custom-select custom-select-sm mb-1" name="type[]">' +
						'<option value="-1" selected>Любой вид</option>' +
					'</select>' +
					'<select class="search-age custom-select custom-select-sm mb-1" name="type[]">' +
						'<option value="-1" selected>Любой возраст</option>' +
					'</select>' +
					'<select class="search-color custom-select custom-select-sm mb-1" name="type[]">' +
						'<option value="-1" selected>Любой цвет</option>' +
					'</select>' +
				'</div>');
		}
	})
	.done(function()
	{
		// Вид (порода) питомцев
		$.post('php/pet-breed.php', function(result) 
		{ 
			breeds = JSON.parse(result);
			
			// Заполняем список видов
			for (var i = 0; i < breeds.length; ++i)
			{
				$.each(breeds[i], function(key, value)
				{
					$('.search-breed:eq('+i+')').append($("<option></option>").attr("value", key).text(value)); 
				});
			}
		});

		// Возраст питомцев
		$.post('php/pet-age.php', function(result) 
		{ 
			ages = JSON.parse(result);
			
			// Заполняем список возрастов
			for (var i = 0; i < ages.length; ++i)
			{
				$.each(ages[i], function(key, value)
				{
					$('.search-age:eq('+i+')').append($("<option></option>").attr("value", key).text(value)); 
				});
			}
		});

		// Цвета питомцев
		$.post('php/pet-color.php', function(result) 
		{ 
			colors = JSON.parse(result);
			
			// Заполняем список цветов
			for (var i = 0; i < colors.length; ++i)
			{
				$.each(colors[i], function(key, value)
				{
					$('.search-color:eq('+i+')').append($("<option></option>").attr("value", key).text(value)); 
				});
			}
		});
	})
	.always(function() 
	{
		$("#form-search-animal").append(
			'<div class="row">' +
				'<div class="col">' +
					'<input type="button" class="btn btn-dark btn-sm btn-block" value="Сбросить" onClick="resetSearch();">' +
				'</div>' +
				'<div class="col">' +
					'<input type="submit" class="btn btn-primary btn-sm btn-block" value="Поиск">' +
				'</div>' +
			'</div>');
	});
	
	/* 
	Обработка события, возникающего при щелчке
	правой кнопки мыши в любой точке карты
	При возникновении такого события покажем всплывающую подсказку
	в точке щелчка
	*/
	myMap.events.add('contextmenu', function (e) 
	{
		if (!myMap.balloon.isOpen()) 
		{
			var coords = e.get('coords');
			var rectCoords = convertFromGCtoRC(coords[0], coords[1]);
	
			// Обратное геокодирование
			// https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode-docpage
			ymaps.geocode(coords, 
			{
				/**
				 * Опции запроса
				 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
				 */
				// Запрашиваем не более 1 результата
				results: 1
			}).then(function (res) 
			{
				// Открываем балун со следующими характеристиками
				myMap.balloon.open(coords, 
				{
					contentHeader: 'Информация об объекте',
					contentBody: '<p class="mb-1">' + res.geoObjects.get(0).properties.get('text') + '</p>' +
					'<p class="mb-1">Географические координаты WGS-84: ' + [
						gaussRound(coords[0], 6),
						gaussRound(coords[1], 6)
					].join(', ') + '</p>' + 
					'<p class="mb-1">Прямоугольные координаты WGS-84: ' + [
						gaussRound(rectCoords[0], 6),
						gaussRound(rectCoords[1], 6)
					].join(', ') + '</p>' +
					'<div class="container"><form method="post" id="form-add-animal" action="javascript:void(null);" onSubmit="addAnimal();">' +	
					'<legend><h5>Добавить питомца</h5></legend>' +
					'<div class="row"><div class="col-4"><label for="add_pet_type">Тип: </label></div>' + 
						'<div class="col-7"><select id="add_pet_type" class="custom-select custom-select-sm mb-1" name="add_pet_type" onchange="updatePetType(this)">' +
							'<option value="0" selected>' + types[0] + '</option>' +
							'<option value="1">' + types[1] + '</option>' +
							'<option value="2">' + types[2] + '</option>' +
							'<option value="3">' + types[3] + '</option>' +
						'</select></div></div>' +
					'<div class="row" style="display: none;"><div class="col-4"><label for="add_coordinates">Координаты: </label></div>' + 
						'<div class="col-7"><input type="text" id="add_coordinates" class="form-control-sm mb-1" name="add_coordinates" value="' + gaussRound(coords[0], 6) + ', ' + gaussRound(coords[1], 6) + '" readonly></div></div>' +
					'<div class="row" style="display: none;"><div class="col-4"><label for="add_address">Адрес: </label></div>' + 
						'<div class="col-7"><input type="text" id="add_address" class="form-control-sm mb-1" name="add_address" value="' + res.geoObjects.get(0).properties.get('text') + '" readonly></div></div>' +	
					'<div class="row"><div class="col-4"><label for="add_breed">Вид: </label></div>' + 
						'<div class="col-7"><select id="add_breed" class="custom-select custom-select-sm mb-1" name="add_breed">' +
							'<option value="0" selected>' + breeds[0][0] + '</option>' +
							'<option value="1">' + breeds[0][1] + '</option>' +
							'<option value="2">' + breeds[0][2] + '</option>' +
							'<option value="3">' + breeds[0][3] + '</option>' +
							'<option value="4">' + breeds[0][4] + '</option>' +
						'</select></div></div>' +
					'<div class="row"><div class="col-4"><label for="add_age">Возраст: </label></div>' + 
						'<div class="col-7"><select id="add_age" class="custom-select custom-select-sm mb-1" name="add_age">' +
							'<option value="0" selected>' + ages[0][0] + '</option>' +
							'<option value="1">' + ages[0][1] + '</option>' +
							'<option value="2">' + ages[0][2] + '</option>' +
						'</select></div></div>' +
					'<div class="row"><div class="col-4"><label for="add_color">Цвет: </label></div>' + 
						'<div class="col-7"><select id="add_color" class="custom-select custom-select-sm mb-1" name="add_color">' +
							'<option value="0" selected>' + colors[0][0] + '</option>' +
							'<option value="1">' + colors[0][1] + '</option>' +
							'<option value="2">' + colors[0][2] + '</option>' +
							'<option value="3">' + colors[0][3] + '</option>' +
							'<option value="4">' + colors[0][4] + '</option>' +
						'</select></div></div>' +
					'<div class="row"><div class="col-4"><label for="add_description">Описание: </label></div>' + 
						'<div class="col-7"><textarea id="add_description" class="form-control-sm mb-1" name="add_description" required></textarea></div></div>' + 
					'<div class="row"><div class="col-4"><label for="add_img_url">Изображение: </label></div>' + 
						'<div class="col-7"><div class="input-group-sm mb-1"><div class="input-group-prepend"><div class="input-group-text">URL</div>' +
						'<input type="text" id="add_img_url" class="form-control-sm" name="add_img_url" value=""></div></div></div></div>' +	
					'<input type="submit" class="btn btn-success btn-sm" value="Отправить"></form></div>',
					contentFooter:'<sup>Кликните еще раз для выхода</sup>'
				});
			});	
		}
		else 
		{
			// Закрываем балун, если тот уже открыт
			myMap.balloon.close();
		}
	});
	   
	// Скрываем хинт при открытии балуна
	myMap.events.add('balloonopen', function (e) 
	{
		myMap.hint.close();
	});

	// Создадим пользовательский макет кнопок масштаба
	ZoomLayout = ymaps.templateLayoutFactory.createClass("<div>" +
		"<div id='zoom-in' class='btn-zoom'><img src='img/ico/plus.png';></div>" +
		"<div id='zoom-out' class='btn-zoom'><img src='img/ico/minus.png'></div>" +
	"</div>", 
	{
		// Переопределяем методы макета, чтобы выполнять дополнительные действия
		// при построении и очистке макета
		build: function () 
		{
			// Вызываем родительский метод build
			ZoomLayout.superclass.build.call(this);

			// Привязываем функции-обработчики к контексту и сохраняем ссылки
			// на них, чтобы потом отписаться от событий.
			this.zoomInCallback = ymaps.util.bind(this.zoomIn, this);
			this.zoomOutCallback = ymaps.util.bind(this.zoomOut, this);

			// Начинаем слушать клики на кнопках макета.
			$('#zoom-in').bind('click', this.zoomInCallback);
			$('#zoom-out').bind('click', this.zoomOutCallback);
		},

		clear: function () 
		{
			// Снимаем обработчики кликов.
			$('#zoom-in').unbind('click', this.zoomInCallback);
			$('#zoom-out').unbind('click', this.zoomOutCallback);

			// Вызываем родительский метод clear.
			ZoomLayout.superclass.clear.call(this);
		},

		zoomIn: function () 
		{
			var map = this.getData().control.getMap();
			map.setZoom(map.getZoom() + 1, {checkZoomRange: true});
		},

		zoomOut: function () 
		{
			var map = this.getData().control.getMap();
			map.setZoom(map.getZoom() - 1, {checkZoomRange: true});
		}
	}),
	zoomControl = new ymaps.control.ZoomControl({options: {layout: ZoomLayout, position: {right: '15px', top: '15px'}}});
	
	myMap.controls.add(zoomControl);
}

/** Процедура прямого геокодирования */
function directGeocoding()
{
	ymaps.geocode($('#inpt-direct-geocoding').val(), 
	{
		/**
		 * Опции запроса
		 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
		 */
		// Сортировка результатов от центра окна карты
		boundedBy: myMap.getBounds(),
		results: 1
	}).then(function (res) 
	{
		// Выбираем первый результат геокодирования.		
		var myGeoObject = res.geoObjects.get(0);

		if (typeof myGeoObject === "undefined") 
		{
			showAlert("alert-warning", "Увы :(", "Ничего не найдено");
			return;
		}
				
        // Координаты геообъекта
        var coords = myGeoObject.geometry.getCoordinates(),
        // Область видимости геообъекта
        bounds = myGeoObject.properties.get('boundedBy');
							
		// Масштабируем карту на область видимости геообъекта.
		myMap.setBounds(bounds, 
		{
			// Проверяем наличие тайлов на данном масштабе
			checkZoomRange: true
		});
				
		// Код ниже для отладки. Можно раскомментировать и посмотреть функции геокодера подробнее
		/*
		console.log('Все данные геообъекта: ', myGeoObject.properties.getAll());
		console.log('Метаданные ответа геокодера: ', res.metaData);
		console.log('Метаданные геокодера: ', myGeoObject.properties.get('metaDataProperty.GeocoderMetaData'));
		console.log('precision', myGeoObject.properties.get('metaDataProperty.GeocoderMetaData.precision'));
		console.log('Тип геообъекта: %s', myGeoObject.properties.get('metaDataProperty.GeocoderMetaData.kind'));
		console.log('Название объекта: %s', myGeoObject.properties.get('name'));
		console.log('Описание объекта: %s', myGeoObject.properties.get('description'));
		console.log('Полное описание объекта: %s', myGeoObject.properties.get('text')); 
		*/
	});
}

/** Процедура отлавливания нажатия клавиши Enter (для поиска гео-объектов) */
function handle(e)
{
	if (e.keyCode === 13)
	{
		e.preventDefault();
		directGeocoding();
	}
}

/** Обновляет породы и виды питомцев в #add_breed по изменению 
	активного пункта в выпадающем списке #add_pet_type
	@param selected option
*/
function updatePetType(sel)
{
	$('#add_breed option').each(function(index, Element)
	{
		$(this).text(breeds[sel.value][index]);
	});
}

/** Процедура добавления питомца в базу данных. Используется в контекстном меню (клик ПКМ) */
function addAnimal()
{
	var msg  = $('#form-add-animal').serialize();
	//Выполняем асинхронный запрос добавления питомца в БД
	$.ajax(
	{
		type: 'POST',
		url: 'php/addmarker.php',
		data: msg,
		success: function(data) 
		{
			showAlert("alert-success", "Питомец успешно добавлен.", "Обновите страницу.");
		},
		error:  function(xhr, str)
		{
			showAlert("alert-danger", "Ошибка.", xhr.responseCode);
		}
	});	 
	
	// Закрываем Балун
	myMap.balloon.close();	
}

/** Поиск питомцев по фильтрам */
function searchAnimal()
{
	var res = new ymaps.GeoQueryResult(),
		arr = $("#search-type").val();
	
	// Записываем в в спец. коллекцию для гео-запросов все маркеры, выгруженные раннее из БД
	window.myObjects = ymaps.geoQuery(myPlacemarks);	
	
	//Циклом выбираем все обозначенные для поиска типы животных и их доп. фильтры
	$.each(arr, function(key, value)
	{
		var selection = myObjects.search('properties.type = "' + value + '"');
		var currentVal = $(".search-breed:eq("+value+")").val();
		if (currentVal != "-1") 
			selection = selection.search('properties.breed = "' + currentVal + '"');
		
		currentVal = $(".search-age:eq("+value+")").val();
		if (currentVal != "-1") 
			selection = selection.search('properties.age = "' + currentVal + '"');
		
		currentVal = $(".search-color:eq("+value+")").val();
		if (currentVal != "-1") 
			selection = selection.search('properties.color = "' + currentVal + '"');
		
		res = res.add(selection);
	});
		
	// Если на карте имеется окружность, поиск выполняется в ней
	if (myCircle != undefined)	 
	{
		res = res.searchInside(myCircle);
		myMap.geoObjects.remove(myCircle);	
		
		// Удаляем окружность
		myCircle = undefined;
	}
	
	// Если на карте имеется полигон, поиск выполняется в нём.
	if (myPolygon != undefined)
	{
		res = res.searchInside(myPolygon);
		myMap.geoObjects.remove(myPolygon);	
		
		// Удаляем полигон
		myPolygon = undefined;
	}	
	
	// Выполняем поиск по адресу (если тот указан)
	// Обратное гео-кодирование работать не будет, т.к. выполняется медленнее и имеет ограничения
	res = res.search('properties.address regExp "'+$("#search-address").val()+'"');	
	
	//Удлаяем все маркеры с карты путём очистки objManager
	objManager.removeAll();
	
	// Для каждого объекта, полученного в результате гео-выборки, проведённой выше,
	// выполняем функцию, добавляющую объекты в objManager
	res.each(function(geoObject, index)
	{
		// objManager работает криво, если встречаются повторяющиеся идентификаторы (даже те, которые раньше были удалены)
		// Во избежании проблем используем такое ad hoc решение в виде смещения идентификаторов ._.
		var magicOffset = 1000000;
		
		objManager.add(
		{
			// Приводим GeoQueryResult к нужному виду для objManager
			type: 'Feature',
			id: index + magicOffset,
			geometry: 
			{
				type: geoObject.geometry.getType(),
				coordinates: geoObject.geometry.getCoordinates()
			},
			properties: geoObject.properties.getAll(),
			options: geoObject.options.getAll()
		});
	});
	
	showAlert("alert-info", "Питомцев найдено:", res.getLength());
}

/** Сбрасывает фильтры поиска питомцев */
function resetSearch()
{
	objManager.removeAll();
	objManager.add(myPlacemarks);
	// Обратить внимание на то, что запрос к БД не осуществляется
}

/** Активирует / деактивирует доп. фильтры */
function filterToggler()
{
	var typesCount = $(".search-breed").length;
	
	// Сначала деактивируем все филтры.
	for (var i = 0; i < typesCount; ++i)
	{
		$(".search-breed:eq("+i+")").prop("disabled", true);   
		$(".search-age:eq("+i+")").prop("disabled", true); 	 
		$(".search-color:eq("+i+")").prop("disabled", true);
	}
	
	var arr = $("#search-type").val();
	
	// Циклом проверяем все выделенные типы животных и активируем нужные фильтры.
	$.each(arr, function(key, value)
	{
		$(".search-breed:eq("+value+")").prop("disabled", false); 
		$(".search-age:eq("+value+")").prop("disabled", false); 
		$(".search-color:eq("+value+")").prop("disabled", false);
	});
}

/** Рисует круг для выборки питомцев */
function drawCircle()
{
	// Удаляем всё, что нарисовали раннее
	deleteDrawings();
	
	// Инициализируем объект круга
	myCircle = new ymaps.Circle([], {}, 
	{
		editorDrawingCursor: "crosshair",
		opacity: 0.5
	});

	// Добавляем объект на карту
	myMap.geoObjects.add(myCircle);
	// Начинаем рисовать	
	myCircle.editor.startDrawing();
}

/** Рисует полигон для выборки питомцев */
function drawPolygon()
{
	// Удаляем всё, что нарисовали раннее
	deleteDrawings();
	
	// Инициализируем объект полигона
	myPolygon = new ymaps.Polygon([], {}, 
	{
		editorDrawingCursor: "crosshair",
		opacity: 0.5
	});

	// Добавляем объект на карту
	myMap.geoObjects.add(myPolygon);
	// Начинаем рисовать
	myPolygon.editor.startDrawing();	
}
	
/** Удаляет всё, что нарисовали раннее */
function deleteDrawings()	
{
	if (myCircle != undefined)
	{
		myMap.geoObjects.remove(myCircle);	
		myCircle = undefined;
	}

	if (myPolygon != undefined)
	{
		myMap.geoObjects.remove(myPolygon);
		myPolygon = undefined;
	}	
}

/** Вручную завершает редактирование графических объектов, по которым выполняется выборка */
function manualStopDrawing()
{
	if (myCircle != undefined) 
		myCircle.editor.stopEditing();	
	
	if (myPolygon != undefined) 
		myPolygon.editor.stopEditing();	
}